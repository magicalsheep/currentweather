package com.magicalsheep.currentweather;

import com.magicalsheep.currentweather.util.TemperatureConverter;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by triin on 12.9.2016.
 */
public class TemperatureConverterTests {

    @Test
    public void temperatureStringGenerationTest_positive() {
        //also testing a single number and a number in range 11-20
        assertEquals("viis kuni viisteist kraadi sooja", TemperatureConverter.temperatureRangeToWords(5, 15));
    }

    @Test
    public void temperatureStringGenerationTest_negative() {
        assertEquals("seitse kraadi külma kuni kuus kraadi sooja", TemperatureConverter.temperatureRangeToWords(-7, 6));
    }

    @Test
    public void temperatureStringGenerationTest_mixed() {
        assertEquals("kakskümmend neli kuni kaheksa kraadi külma", TemperatureConverter.temperatureRangeToWords(-24, -8));
    }

}
