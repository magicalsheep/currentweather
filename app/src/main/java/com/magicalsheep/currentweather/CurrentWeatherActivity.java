package com.magicalsheep.currentweather;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.content.Context;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.content.res.Resources.Theme;

import android.widget.TextView;
import android.widget.Toast;

import com.magicalsheep.currentweather.util.ResourceConverter;
import com.magicalsheep.currentweather.util.TemperatureConverter;
import com.magicalsheep.currentweather.util.XmlParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class CurrentWeatherActivity extends AppCompatActivity {
    private final static String FEED_URL = "http://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php";
    public static List<Weather> weatherList; //Has general weather
    public static HashMap<String, Place> places; //Has weather information for specific locations
    public static ResourceConverter pConverter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initiating class fields
        weatherList = new LinkedList<Weather>();
        places = new HashMap<String, Place>();
        pConverter = new ResourceConverter(this);

        setUpWeatherView();
        tryFetchFeed();
        //Note: weather info added at fragment's onCreateView

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_current_weather, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Saving chosen location when user leaves
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Spinner placeSpinner = (Spinner) findViewById(R.id.place_spinner);
        int selectedItem = placeSpinner.getSelectedItemPosition();

        editor.putInt("location", selectedItem);
        editor.commit();
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        int selectedLocation = sharedPref.getInt("location", -1);
        if(selectedLocation != -1){
            Log.d("CurrentWeatherActivity", "onRestart(): Retrieved location from preferences");
            Spinner placeSpinner = (Spinner) findViewById(R.id.place_spinner);
            placeSpinner.setSelection(selectedLocation);
        }

    }

    private static class MyAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter {
        private final ThemedSpinnerAdapter.Helper mDropDownHelper;

        public MyAdapter(Context context, String[] objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                // Inflate the drop down using the helper's LayoutInflater
                LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(getItem(position));

            return view;
        }

        @Override
        public Theme getDropDownViewTheme() {
            return mDropDownHelper.getDropDownViewTheme();
        }

        @Override
        public void setDropDownViewTheme(Theme theme) {
            mDropDownHelper.setDropDownViewTheme(theme);
        }
    }


    /**
     * A fragment containing a simple view.
     */
    public static class WeatherFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        public static String SELECTED_DATE;

        public WeatherFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static WeatherFragment newInstance(int sectionNumber) {
            WeatherFragment fragment = new WeatherFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_current_weather, container, false);

            return rootView;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            //Set values for weather-related UI components, using values from weatherList
            addWeatherInfo();
            if(!places.isEmpty()){
                addPlaceSpinner(); //Spinner to choose between locations
            }
        }

        /**
         * Adds weather info to Weather fragment (weatherList to UI)
         */
        public void addWeatherInfo(){
            View rootView = this.getView().getRootView();

            NightWeather night = null;
            DayWeather day = null;

            Spinner mySpinner=(Spinner) rootView.findViewById(R.id.spinner);
            if(mySpinner == null){
                Log.e("CurrentWeatherActivity", "Spinner is null!");
            }else{
                String chosenDate = mySpinner.getSelectedItem().toString();
                Log.d("CurrentWeatherActivity", "Chosen date is " + chosenDate);
            }

            if(weatherList == null) {
                Log.d("CurrentWeatherActivity", "addWeatherInfo: WeatherList is null!");
            }else if(weatherList.isEmpty()){
                Log.d("CurrentWeatherActivity", "addWeatherInfo: weatherList is empty!");
            }else{
                DateFormat formatter = new SimpleDateFormat("dd.MM");
                for(Weather w: weatherList){
                    String dateAsString = formatter.format(w.date);
                    if(dateAsString.equals(SELECTED_DATE)){
                        if(w instanceof NightWeather){
                            night = (NightWeather) w;
                        } else {
                            day = (DayWeather) w;
                        }
                    }
                }

                //Image for day weather
                ImageView dayImageView = (ImageView) rootView.findViewById(R.id.image_day);
                dayImageView.setImageResource(ResourceConverter.getDayImageId(day.phenomenonIndex));

                //Temperature
                TextView temperatureView = (TextView) rootView.findViewById(R.id.textView_temperature);
                temperatureView.setText(day.minTemp + "..." + day.maxTemp + " °C");

                String dayTempInWords = TemperatureConverter.temperatureRangeToWords(day.minTemp, day.maxTemp);
                TextView temperatureWordsView = (TextView) rootView.findViewById(R.id.textView_temperature_words);
                temperatureWordsView.setText(dayTempInWords);

                //If minWind is set to POSITIVE_INFINITY (the initial value), then it means that
                // there were no wind elements for this day/night and therefore wind cannot be displayed.
                if(night.minWind != XmlParser.POSITIVE_INFINITY) {
                    TextView windView = (TextView) rootView.findViewById(R.id.textView_wind_day);
                    windView.setText("Tuul " + day.minWind + "-" + day.maxWind + "m/s");
                }

                //Longer description of general weather for day
                TextView descriptionView = (TextView) rootView.findViewById(R.id.textView_description_day);
                descriptionView.setText("Päevase ilma kirjeldus: " + day.description);

                //Image for night weather
                ImageView nightImageView = (ImageView) rootView.findViewById(R.id.image_night);
                nightImageView.setImageResource(ResourceConverter.getNightImageId(night.phenomenonIndex));

                //Night temperature
                TextView nightTemperatureView = (TextView) rootView.findViewById(R.id.textView_temperature_night);
                nightTemperatureView.setText(night.minTemp + "..." + night.maxTemp + " °C");

                String nightTempInWords = TemperatureConverter.temperatureRangeToWords(night.minTemp, night.maxTemp);
                TextView nightTemperatureWordsView = (TextView) rootView.findViewById(R.id.textView_temperature_words_night);
                nightTemperatureWordsView.setText(nightTempInWords);

                //If minWind is set to POSITIVE_INFINITY (the initial value), then it means that
                // there were no wind elements for this day/night and therefore wind cannot be displayed.
                if(night.minWind != XmlParser.POSITIVE_INFINITY){
                    TextView nightWindView = (TextView) rootView.findViewById(R.id.textView_wind_night);
                    nightWindView.setText("Tuul " + night.minWind + "-" + night.maxWind + " m/s");
                }

                //Longer description of general weather for night
                TextView nightDescriptionView = (TextView) rootView.findViewById(R.id.textView_description_night);
                nightDescriptionView.setText("Öise ilma kirjeldus: " + night.description);
            }
        }

        private void addPlaceSpinner(){
            final View rootView = this.getView().getRootView();

            //Locations to display in spinner
            String[] spinnerPlaces = places.keySet().toArray(new String[places.size()+1]);
            spinnerPlaces[spinnerPlaces.length-1] = getString(R.string.whole_estonia);

            // SET UP SPINNER
            Spinner placeSpinner = (Spinner) rootView.findViewById(R.id.place_spinner);
            Spinner dateSpinner = (Spinner)rootView.findViewById(R.id.spinner);

            //Don't let the user choose local weather for anything other than first day
            if(dateSpinner.getSelectedItemId() != 0){
                placeSpinner.setEnabled(false);
            } else {
                placeSpinner.setAdapter(new MyAdapter(
                        this.getContext(),
                        spinnerPlaces));
                placeSpinner.setSelection(spinnerPlaces.length-1);
                placeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        // When a place is chosen, show corresponding local weather
                        String selectedItem = parent.getSelectedItem().toString();
                        Spinner dateSpinner = (Spinner) view.getRootView().findViewById(R.id.spinner);

                        if(selectedItem.equals(getString(R.string.whole_estonia))){
                            showGeneralWeather();
                        }else{
                            //Local weather is only available for the first date
                            if(dateSpinner.getSelectedItemId() == 0){
                                showLocalWeather(selectedItem);
                            } else {
                                hideLocalWeather();
                            }

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }

                    private void showLocalWeather(String selectedItem){
                        View generalWeatherView = rootView.findViewById(R.id.general_weather);
                        generalWeatherView.setVisibility(View.GONE);
                        View localWeatherView = rootView.findViewById(R.id.local_weather);
                        localWeatherView.setVisibility(View.VISIBLE);

                        NightWeather localNight = places.get(selectedItem).getNight();
                        DayWeather localDay = places.get(selectedItem).getDay();

                        ImageView localDayView = (ImageView) rootView.findViewById(R.id.image_local_day);
                        ImageView localNightView = (ImageView) rootView.findViewById(R.id.image_local_night);

                        localDayView.setImageResource(ResourceConverter.getDayImageId(localDay.phenomenonIndex));
                        localNightView.setImageResource(ResourceConverter.getNightImageId(localNight.phenomenonIndex));

                        TextView localTemp = (TextView) rootView.findViewById(R.id.textView_local_temperature);
                        localTemp.setText(localNight.minTemp + "..." + localDay.maxTemp + " °C");

                        TextView localPhenomenon = (TextView) rootView.findViewById(R.id.textView_local_phenomenon);
                        localPhenomenon.setText("Päeval: " + localDay.phenomenon + "\nÖösel: " + localNight.phenomenon + ".");
                    }

                    private void showGeneralWeather(){
                        View generalWeatherView = rootView.findViewById(R.id.general_weather);
                        generalWeatherView.setVisibility(View.VISIBLE);
                        hideLocalWeather();
                    }

                    private void hideLocalWeather(){
                        View localWeatherView = rootView.findViewById(R.id.local_weather);
                        localWeatherView.setVisibility(View.GONE);
                    }
                });
            }

        }
    }

    /***
     * Tries to fetch the weather forecast xml file, fails and displays a message in case of network problems
     * @return whether downloading succeeded
     */
    private boolean tryFetchFeed(){
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            new DownloadXmlTask().execute(FEED_URL);
            return true;
        } else {
            Toast toast = Toast.makeText(this, getString(R.string.connection_error_toast), Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }

    }

    /***
     * Making network transactions asynchronous
     */
    private class DownloadXmlTask extends AsyncTask<String, Void, List<Weather>> {
        @Override
        protected List<Weather> doInBackground(String... urls) {
            try {
                return loadXmlFromNetwork(urls[0]);
            } catch (IOException e) {
                Log.e("DownloadXmlTask", "IO Exception at doInBackground");
                e.printStackTrace();
                return null;
            } catch (XmlPullParserException e) {
                Log.e("DownloadXmlTask", "Problem with XmlPullParser at DownloadXmlTask: doInBackground");
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Weather> result) {
            CurrentWeatherActivity.weatherList = result;
            setUpWeatherView();
            WeatherFragment f = (WeatherFragment) getSupportFragmentManager().findFragmentById(R.id.container);
            if(f != null){
                f.addWeatherInfo();
            }else{
                Log.d("CurrentWeatherActivity", "onPostExecute: WeatherFragment is null!");
            }
        }
    }

    /**
     * Downloads the XML file and sends it to XmlPullParser for parsing
     * Might still need refactoring
     * @param urlString URL of the XML feed to download
     * @return A list of Weather entries parsed from XML, more specifically instances of DayWeather and NightWeather
     * @throws XmlPullParserException
     * @throws IOException
     */
    private List<Weather> loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
        InputStream stream = null;
        List<Weather> list;
        // Instantiate the parser
        XmlParser xmlParser = new XmlParser();

        try {
            stream = downloadUrl(urlString);
            list = xmlParser.parse(stream);
            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return list;
    }

    /***
     * Given a string representation of a URL, sets up a connection and gets
     * an input stream.
     * @param urlString location of the weather RSS feed
     * @return InputStream with XML to be parsed
     * @throws IOException
     */
    //
    private InputStream downloadUrl(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Starts the query
        conn.connect();
        return conn.getInputStream();
    }

    /***
     * Sets up the more specific UI components of CurrentWeatherActivity (toolbars, sections, listeners, buttons)
     */
    private void setUpWeatherView(){
        setContentView(R.layout.activity_current_weather);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        DateFormat formatter = new SimpleDateFormat("dd.MM");


        String[] spinnerDates;
        if(weatherList.isEmpty()){
            Log.d("CurrentWeatherActivity", "setUpWeatherView: Cannot get weather dates from XML, generating");
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 1);
            spinnerDates = new String[4];
            for(int i = 0; i < 4; i++){
                spinnerDates[i] = formatter.format(c.getTime());
                c.add(Calendar.DATE, 1);
            }
        } else {
            LinkedList<String> dateList = new LinkedList<String>();
           for(Weather w: weatherList){
               String dateString = formatter.format(w.date);
               if(!dateList.contains(dateString)){
                   dateList.add(dateString);
               }
           }
            spinnerDates = dateList.toArray(new String[dateList.size()]);
        }

        // Setup spinner
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(new MyAdapter(
                toolbar.getContext(),
                spinnerDates));

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // When the given dropdown item is selected, show its contents in the
                // container view.
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, WeatherFragment.newInstance(position + 1))
                        .commit();

                String selectedItem = parent.getSelectedItem().toString();
                WeatherFragment.SELECTED_DATE = selectedItem;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }
}
