package com.magicalsheep.currentweather;

/**
 * Created by triin on 15.9.2016.
 */
public class Place {
    private String name;
    private NightWeather night;
    private DayWeather day;

    public Place(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public NightWeather getNight() {
        return night;
    }

    public DayWeather getDay() {
        return day;
    }

    public void setNight(NightWeather night) {
        this.night = night;
    }

    public void setDay(DayWeather day) {
        this.day = day;
    }
}

