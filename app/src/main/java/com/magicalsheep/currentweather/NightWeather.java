package com.magicalsheep.currentweather;

import java.util.Date;

/**
 * Created by triin on 8.9.2016.
 */
public class NightWeather extends Weather {

    public NightWeather(Date date, int minTemp, int maxTemp, String description, int minWind, int maxWind, String phenomenon, int phenomenonIndex) {
        super(minTemp, maxTemp, description, minWind, maxWind, date, phenomenon, phenomenonIndex);

    }

    public NightWeather(Date date, int minTemp, String phenomenon, int phenomenonIndex){
        this.date = date;
        this.minTemp = minTemp;
        this.phenomenon = phenomenon;
        this.phenomenonIndex = phenomenonIndex;
    }
}
