package com.magicalsheep.currentweather;

import java.util.Date;

/**
 * Created by triin on 8.9.2016.
 */
public class DayWeather extends Weather {

    public DayWeather(Date date, int minTemp, int maxTemp, String description, int minWind, int maxWind, String phenomenon, int phenomenonIndex){
        super(minTemp, maxTemp, description, minWind, maxWind, date, phenomenon, phenomenonIndex);
    }

    public DayWeather(Date date, int maxTemp, String phenomenon, int phenomenonIndex){
        this.date = date;
        this.maxTemp = maxTemp;
        this.phenomenon = phenomenon;
        this.phenomenonIndex = phenomenonIndex;
    }

}
