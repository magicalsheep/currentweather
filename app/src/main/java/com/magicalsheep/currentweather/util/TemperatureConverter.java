package com.magicalsheep.currentweather.util;

/**
 * Created by triin on 12.9.2016.
 */
public class TemperatureConverter {
    static String[] numbers = {"null", "üks", "kaks", "kolm", "neli", "viis", "kuus", "seitse", "kaheksa", "üheksa", "kümme"};

    public static String temperatureRangeToWords(int min, int max){
        String result = intToWords(min);

        //If min is negative and max is positive. Cannot logically be the opposite
        if(!sameSign(min, max)){
            result += " kraadi külma";
        }
        result += " kuni ";
        result += intToWords(max);
        //If one or both are positive. This check is enough because if only max is positive, 'kraadi külma' was already added before
        // and if both are positive, then adding 'kraadi sooja' here is enough.
        if(max>0){
            result += " kraadi sooja";
        //if max is not positive, then min definitely isn't either
        }else{
            result += " kraadi külma";
        }
        return result;
    }

    /***
     * Converts a single number into an Estonian word (or two).
     * Meant for weather temperatures in Celsius, so handles max 2 digits and a minus sign
     * @param t number to convert to words
     * @return Estonian word(s) equivalent to the int given as argument
     */
    private static String intToWords(int t){
        int tAbs = Math.abs(t);
        String result = "";
        String tAbsAsString = Integer.toString(Math.abs(t));

        if(tAbs<11){
            //if number is smaller than 11, then simple retrieval by absolute value will do
            result += numbers[tAbs];

        //From 11 and up, word string has to be constructed from multiple parts
        //Numbers between 11 and 20
        } else if(tAbs >= 11 && tAbs < 20){
            int secondNumber = Character.getNumericValue(tAbsAsString.charAt(1));
            //Adding the first number digit
            result += numbers[secondNumber];
            result += "teist";

        //Numbers 20 and up
        }else{
            int firstNumber = Character.getNumericValue(tAbsAsString.charAt(0));
            result += numbers[firstNumber];

            result += "kümmend";

            //Adding the second number digit
            result += " ";
            int secondNumber = Character.getNumericValue(tAbsAsString.charAt(1));
            if(secondNumber != 0){
                result += numbers[secondNumber];
            }
        }
        return result;
    }

    /***
     * Checks whether two numbers have the same sign (+ or -).
     * True if both positive/negative, false otherwise.
     * @param min smaller number
     * @param max larger number
     * @return true if either both numbers are negative or both are positive
     */
    private static boolean sameSign(int min, int max){
        //True if both are positive (or rather, non-negative)
        if(min >= 0 && max >= 0){
            return true;
        //true if both negative
        }else if(min < 0 && max < 0){
            return true;
        }else{
            return false;
        }
    }
}
