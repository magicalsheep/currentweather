package com.magicalsheep.currentweather.util;

import android.util.Log;
import android.util.Xml;

import com.magicalsheep.currentweather.CurrentWeatherActivity;
import com.magicalsheep.currentweather.DayWeather;
import com.magicalsheep.currentweather.NightWeather;
import com.magicalsheep.currentweather.Place;
import com.magicalsheep.currentweather.Weather;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by triin on 8.9.2016.
 */
public class XmlParser {
    // We don't use namespaces
    private static final String ns = null;
    private static final String TAG = "XmlParser";
    //1000 should be fine for wind speed and temperature, especially a minimum one.
    public static int POSITIVE_INFINITY = 1000;
    private int maxWind = 0;
    private int minWind = POSITIVE_INFINITY;

    public List<Weather> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }

    private List<Weather> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Weather> entries = new ArrayList<Weather>();

        parser.require(XmlPullParser.START_TAG, ns, "forecasts");
        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the forecast tag
            if (name.equals("forecast")) {
                //Retrieve date
                String dateAsString = parser.getAttributeValue(0);
                Date date = readDate(dateAsString);

                //Next tag in order to go into forecast element
                parser.nextTag();
                //If next inner tag is day, then parseDay, else parseNight
                String time = parser.getName();
                if (time.equals("night")) {
                    //New day/night: initialize wind variables
                    maxWind = 0;
                    minWind = POSITIVE_INFINITY;
                    entries.add(readWeather(parser, date));
                } else {
                    Log.e(TAG, "Should have been 'night', but was " + time);
                }

                //Go to next tag
                parser.nextTag();
                time = parser.getName();
                if (time.equals("day")) {
                    //New day/night: initialize wind variables
                    maxWind = 0;
                    minWind = POSITIVE_INFINITY;
                    entries.add(readWeather(parser, date));
                    parser.next();
                } else {
                    Log.e(TAG, "Should have been 'day', but was " + time);
                }

            } else {
                //Should not happen
                Log.e(TAG, "No forecast!");
                skip(parser);
            }
        }
        return entries;
    }

    private Date readDate(String s) throws IOException, XmlPullParserException {
        Date date = null;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = formatter.parse(s);
        } catch (ParseException e) {
            Log.e(TAG,"Failed to parse date from " + s);
            e.printStackTrace();
        }

        if(date != null){
            return date;
        }else{
            return null;
        }
    }

    private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "text");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "text");
        return title;
    }


    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
    private int readNumber(XmlPullParser parser) throws IOException, XmlPullParserException {
        int result = 0;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Integer.parseInt(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    private Weather readWeather(XmlPullParser parser, Date date) throws IOException, XmlPullParserException {
        String description = "";
        int tempMin = 0;
        int tempMax = 0;
        String phenomenon = "";
        int phenomenonIndex = -1;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("text")) {
                description = readDescription(parser);
                //parser.nextTag();
            } else if (name.equals("tempmin")) {
                tempMin = readNumber(parser);
                //parser.nextTag();
            } else if (name.equals("tempmax")) {
                tempMax = readNumber(parser);
                //parser.nextTag();
            } else if (name.equals("phenomenon")) {
                phenomenonIndex = ResourceConverter.getPhenomenonIndex(readText(parser));
                phenomenon = ResourceConverter.getEstonianPhenomenon(phenomenonIndex);
            } else if (name.equals("day")) {
                //New day/night: initialize wind variables
                maxWind = 0;
                minWind = POSITIVE_INFINITY;
                readWeather(parser, date);
            } else if(name.equals("wind")){
                int currentMinWind;
                int currentMaxWind;
                //Go in
                while(parser.next() != XmlPullParser.END_TAG){
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    name = parser.getName();
                    if(name.equals("speedmin")){
                        currentMinWind = readNumber(parser);
                        if(currentMinWind < minWind){
                            minWind = currentMinWind;
                        }
                    }else if(name.equals("speedmax")){
                        currentMaxWind = readNumber(parser);
                        if(currentMaxWind > maxWind){
                            maxWind = currentMaxWind;
                        }
                    }else{
                        skip(parser);
                    }
                }
            } else if(name.equals("place")){
                String placeName = "";
                String placePhenomenon = "";
                int placePhenomenonIndex = -1;
                int placeTempMin = POSITIVE_INFINITY;
                int placeTempMax = 0;

                while(parser.next() != XmlPullParser.END_TAG){
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    name = parser.getName();
                    if(name.equals("name")){
                        placeName = readText(parser);
                    }else if(name.equals("phenomenon")){
                        String placePhenomenonEng = readText(parser);
                        placePhenomenonIndex = ResourceConverter.getPhenomenonIndex(placePhenomenonEng);
                        placePhenomenon = ResourceConverter.getEstonianPhenomenon(placePhenomenonIndex);
                    }else if(name.equals("tempmin")){
                        placeTempMin = readNumber(parser);

                        //TODO: Replace new Date with actual date (for correctness)
                        // (Should make no functional difference because we know that local
                        //forecasts only exist for first day)
                        //Night is before day, so Place object for that date doesn't exist yet
                        Place place = new Place(placeName);
                        NightWeather w = new NightWeather(new Date(), placeTempMin, placePhenomenon, placePhenomenonIndex);
                        place.setNight(w);
                        CurrentWeatherActivity.places.put(placeName, place);
                    }else if(name.equals("tempmax")){
                        placeTempMax = readNumber(parser);

                        //TODO: Replace new Date with actual date
                        // Day is after night, so place already exists, just need to add DayWeather
                        DayWeather w = new DayWeather(new Date(), placeTempMax, placePhenomenon, placePhenomenonIndex);
                        Place place = CurrentWeatherActivity.places.get(placeName);
                        place.setDay(w);
                    }else{
                        skip(parser);
                    }
                }

            } else {
                skip(parser);
            }
        }
        String name = parser.getName();
        if (name.equals("day")) {
            Log.d("XmlParser", "Returned new DayWeather with tempmin " + tempMin + ", tempMax " + tempMax + ", description " + description + " and minWind " + minWind);
            return new DayWeather(date, tempMin, tempMax, description, minWind, maxWind, phenomenon, phenomenonIndex);
        }else if(name.equals("night")){
            Log.d("XmlParser", "Returned new NightWeather with tempmin " + tempMin + ", tempMax " + tempMax + " , description " + description + " and minWind " + minWind);
            return new NightWeather(date, tempMin, tempMax, description, minWind, maxWind, phenomenon, phenomenonIndex);
        }else{
            Log.d("XmlParser", "Name was supposed to be day or night, but was " + name.toString());
            return null;
        }
    }



}
