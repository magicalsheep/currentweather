package com.magicalsheep.currentweather.util;

import android.content.Context;
import android.content.res.TypedArray;

import com.magicalsheep.currentweather.R;

/**
 * Created by triin on 15.9.2016.
 */
public class ResourceConverter {
    private static Context context;
    public ResourceConverter(Context context){
        this.context = context;
    }

    /**
     * Gets phenomenon index based on an English phenomenon description. Index applies for
     * R.array.phenomenon_eng, R.array.phenomenon_est, R.array.images_day and R.array.images_night.
     * @param engString phenomenon description in English
     * @return index of the phenomenon in Estonian and English phenomenon arrays, as well as
     * the arrays of phenomenon images
     */
    public static int getPhenomenonIndex(String engString){
        String[] engStrings = context.getResources().getStringArray(R.array.phenomenon_eng);
        for(int i = 0; i < engStrings.length; i++){
            if(engStrings[i].equals(engString)){
                return i;
            }
        }
        return -1;

    }
    /**
     * Cannot be called before PhenomenonConverter is instantiated!
     * @return Estonian string for phenomenon at given index
     */
    public static String getEstonianPhenomenon(int index){
        String[] estStrings = context.getResources().getStringArray(R.array.phenomenon_est);
            return estStrings[index];

    }

    /**
     * Gets the night image of a phenomenon based on index. Index can be aqcuired
     * from gePhenomenonIndex using the English description of the phenomenon.
     * @param index of the phenomenon in arrays
     * @return id of night phenomenon image (like R.id.name).
     */
    public static int getNightImageId(int index){
        TypedArray images = context.getResources().obtainTypedArray(R.array.images_night);
        return images.getResourceId(index,0);
    }
    /**
     * Gets the day image of a phenomenon based on index. Index can be aqcuired
     * from gePhenomenonIndex using the English description of the phenomenon.
     * @param index of the phenomenon in arrays
     * @return id of day phenomenon image (like R.id.name).
     */
    public static int getDayImageId(int index){
        TypedArray images = context.getResources().obtainTypedArray(R.array.images_day);
        return images.getResourceId(index,0);
    }
}
