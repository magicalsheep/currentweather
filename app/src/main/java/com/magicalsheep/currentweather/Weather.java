package com.magicalsheep.currentweather;

import java.util.Date;

/**
 * Created by triin on 8.9.2016.
 */
public abstract class Weather {
    int minTemp;
    int maxTemp;
    String description;
    int minWind;
    public int maxWind;
    public Date date;
    public String phenomenon;
    public int phenomenonIndex;

    public Weather() {
    }

    protected Weather(int minTemp, int maxTemp, String description, int minWind, int maxWind, Date date, String phenomenon, int phenomenonIndex){
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.description = description;
        this.minWind = minWind;
        this.maxWind = maxWind;
        this.date = date;
        this.phenomenon = phenomenon;
        this.phenomenonIndex = phenomenonIndex;
    }

    public void setDate(Date date){
        this.date = date;
    }

}
